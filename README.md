# RWTH Informatik Black Market

## Tools

- [SyncMyL2P](https://www.syncmyl2p.de/) ✝
- [rwth\_cli](https://crates.io/crates/rwth_cli) (unfinished, not recommended for use)
- [SyncMyMoodle](https://github.com/Romern/syncMyMoodle)

## Telegram

### Gruppen und Kanäle

- [Hauptgruppe](https://t.me/RWTHInformatik)
- [Kanal mit einer Liste aller RWTH Informatik-Gruppen](https://t.me/RWTHGruppen)

### Bots

- Mensa-Bot
	- [Telegram](t.me/rwthfressbot)
	- [Repository](https://git.rwth-aachen.de/h/mensa_bot)


- Mentoringkeller-Bot
	- [Telegram](t.me/MentoringkellerBot)
	- [Repository](https://gitlab.com/BergiuTelegram/MentoringkellerBot/)


- Dartbot
	- [Telegram](https://t.me/rwth_dartbot)
	- [Repository](https://github.com/whentze/dartbot)


## Altklausuren, Vorlesungsnotizen, Lösungen

- [S-Inf.de](http://s-inf.de/)
- [Panikzettel](https://panikzettel.philworld.de/)
- [Watercrystals alte Hausaufgaben](http://upload.watercrystal.net/)
- [Codescape-Lösungen](https://github.com/juliusrickert/rwth-codescape)
- [Schrank der Fachschaft](https://www.dropbox.com/sh/3ekad5deex4a1yg/AAC_wPwR4EXm2Et4VJ9mOQrAa?dl=0)

## RWTH-Infrastruktur

- [RWTH OAuth 2.0 documentation](https://oauth.campus.rwth-aachen.de/doc/)
- [L2P API documentation](https://www3.elearning.rwth-aachen.de/_vti_bin/l2pservices/api.svc/v1/documentation)
- [CO API documentation](https://moped.ecampus.rwth-aachen.de/proxy/api/v2/documentation)
- [WLAN monitoring](https://noc-portal.rz.rwth-aachen.de/mops-admin/coverage)
- [Störungsmeldungen](https://maintenance.rz.rwth-aachen.de/ticket/status/messages)

## Misc

- [Weitere Liste mit RWTH-Links](https://github.com/juliusrickert/rwth-links)
- [Abgabepartnerbörse](https://abgabepartner.de/)
- [Latex-Template für Abgaben](https://github.com/sour-dough/solution-tex)
